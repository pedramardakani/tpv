# Build the input dataset while test/developing non-linear warp.
#
# This Makefile is inspired by the 'input-setup-oversampling.sh'
# script created by Mohammad Akhlaghi <mohammad@akhlaghi.org> and
# Natali Anzanello <natali.anzanello@ufrgs.br>.
#
# Original author:
#     Pedram Ashofteh Ardakani <pedramardakani@pm.me>
# Contributing author(s):
# Copyright (C) 2021, Free Software Foundation, Inc.
#
# Gnuastro is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Gnuastro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gnuastro. If not, see <http://www.gnu.org/licenses/>.





# ATTENTION
#
# Running make in parallel can cause undefined behavior since we are using
# '.ONESHELL' and using 'export' to set-up random seeds! So be very careful
# with commands like 'make -j8' with this file!





# Basic Make configurations
include *.conf
SHELL = /bin/bash
SHELLFLAGS = -e -c

all: final
.ONESHELL:
.PHONY: all final





# Create a set of input parameters for the stars in the image with columns:
#   1) X axis position. 2) Y axis position. 3) Magnitude.
input-params=$(indir)/input-params.txt
$(input-params): mock.conf | $(indir)
	export GSL_RNG_SEED=$(star-seed)
	echo $(image-width) \
	     | awk '{for(i=0;i<$(number-stars);++i) print $$1/2, 0}' \
	     | asttable -c'arith $$1 $(image-width) mknoise-uniform' \
	     -c'arith $$1 $(image-width) mknoise-uniform' \
	     -c'arith $(magnitude-faint) \
	              $$2 $(magnitude-sigma) mknoise-sigma abs -' \
	     --envseed --output=$@ \
	     --colmetadata=ARITH_1,X,pix,"X axis position." \
	     --colmetadata=ARITH_2,Y,pix,"Y axis position." \
	     --colmetadata=ARITH_5,MAGNITUDE,log,"Magnitude."





# Create the mock image (and kernel).
nonoise-base=no-noise-no-conv
nonoise-noconv=$(indir)/$(nonoise-base).fits
$(nonoise-noconv): $(input-params) wcs.conf mock.conf | $(indir)
#	We are putting the PSF profile as the first row (with a profile
#	code of 3) and adding all the profiles determined above after it.
	awk 'BEGIN{print 0,  0,  0,  3, 5, 0, 0, 1, 1, 10} \
	     !/^#/{print ++c, $$1, $$2, 4, 1, 0, 0, 0, $$3, 1}' $< \
	    | astmkprof --pc=$(pc)       \
	                --output=$@      \
	                --prepforconv    \
	                --ctype=$(ctype) \
	                --cunit=$(cunit) \
	                --zeropoint=$(zeropoint)  \
	                --cdelt=$(cdelt),$(cdelt) \
	                --crpix=$(crpix),$(crpix) \
	                --crval=$(crval),$(crval) \
	                --mergedsize=$(image-width),$(image-width)

# The kernel is built as a separate image with a certain naming convention,
# so to simplify next steps, we'll just call it 'kernel'.
kernel=$(indir)/0_$(nonoise-base).fits
$(kernel): $(nonoise-noconv) | $(indir)





# Convolve the image with the PSF.
nonoise=$(indir)/nonoise.fits
$(nonoise): $(nonoise-noconv) $(kernel) | $(indir)
	astconvolve $< --kernel=$(kernel) --output=$@





# Warp the image back to the original size.
nonoise-warped=$(indir)/nonoise-warped.fits
$(nonoise-warped): $(nonoise) | $(indir)
	astwarp --scale=1/5 --centeroncorner $< --output=$@





# Crop the extra pixels due to the convolution on the frequency domain.
nonoise-fixed=$(indir)/nonoise-fixed.fits
$(nonoise-fixed): $(nonoise-warped) | $(indir)
	astcrop $< --section=26:*-25,26:*-25 --mode=img --zeroisnotblank \
	        --output=$@





# Add Poisson noise to the image.
input-image=$(indir)/input-image.fits
$(input-image): $(nonoise-fixed) mock.conf | $(indir)
	export GSL_RNG_SEED=$(noise-seed)
	astarithmetic $< $(background-poisson) $(zeropoint) mag-to-counts \
	              mknoise-poisson --output=$@ --envseed





# Detect the signal and do segmentation over the image.
nc=$(indir)/noisechisel.fits
$(nc): $(input-image) | $(indir)
	astnoisechisel $< --output=$@

seg=$(indir)/segmentation.fits
$(seg): $(nc) | $(indir)
	astsegment $< --snquant=0.9 --output=$@





# Measure the object and clump positions and magnitudes.
raw-cat=$(indir)/catalog-raw.fits
$(raw-cat): $(seg) mock.conf | $(indir)
	astmkcatalog $< --zeropoint=$(zeropoint) --clumpscat \
	             --ids --x --y --ra --dec --magnitude --output=$@





# Prepare the input X-Y and measured magnitude catalog.
final-xy=$(indir)/x-y.txt
$(final-xy): $(raw-cat) | $(indir)
	asttable $< --hdu=CLUMPS --column=X,Y,MAGNITUDE --output=$@





# Prepare the input RA, Dec and ideal magnitude catalog.
final-radec=$(indir)/ra-dec.txt
$(final-radec): $(input-params) $(input-image) | $(indir)
	asttable $< -c'arith $$1 $$2 img-to-wcs',MAGNITUDE   \
	         --wcsfile=$(input-image) --output=$@        \
	         --colmetadata=RA---TAN,RA,"Right Ascension" \
	         --colmetadata=DEC--TAN,DEC,"Declination"






tpv-image=$(tpvdir)/tpv-mock.fits
$(tpv-image): $(input-image) | $(tpvdir)
	cp $< $@


distort: $(tpv-image) tpv.conf | $(tpvdir)
	astfits $<                        \
	        --update=PV1_0,$(PV1_0)   \
	        --update=PV1_1,$(PV1_1)   \
	        --update=PV1_2,$(PV1_2)   \
	        --update=PV1_4,$(PV1_4)   \
	        --update=PV1_5,$(PV1_5)   \
	        --update=PV1_6,$(PV1_6)   \
	        --update=PV1_7,$(PV1_7)   \
	        --update=PV1_8,$(PV1_8)   \
	        --update=PV1_9,$(PV1_9)   \
	        --update=PV1_10,$(PV1_10) \
	        --update=PV2_0,$(PV2_0)   \
	        --update=PV2_1,$(PV2_1)   \
	        --update=PV2_2,$(PV2_2)   \
	        --update=PV2_4,$(PV2_4)   \
	        --update=PV2_5,$(PV2_5)   \
	        --update=PV2_6,$(PV2_6)   \
	        --update=PV2_7,$(PV2_7)   \
	        --update=PV2_8,$(PV2_8)   \
	        --update=PV2_9,$(PV2_9)   \
	        --update=PV2_10,$(PV2_10) \
	        --update=CTYPE1,RA---TPV  \
	        --update=CTYPE2,DEC--TPV  \
	        --update=RADECSYS,ICRS    \
	        --update=WCSDIM,2  \
	        --update=CRPIX1,4200 \
	        --update=CRPIX2,4300 \
	        --update=CD1_1,-1.833074903884E-06 \
	        --update=CD2_1,7.239017109944E-05  \
	        --update=CD1_2,7.34840262948E-05   \
	        --update=CD2_2,1.944488720211E-07  \
	        --delete=CDELT1 --delete=CDELT2 --delete=RADESYS \
	        --delete=PC1_1 --delete=PC1_2 --delete=PC2_1 --delete=PC2_2




aligned-image=$(tpvdir)/aligned.fits
$(aligned-image): $(tpv-image) warp.conf | $(tpvdir)
	$(warp) $< -E0 --numthreads=8 -h1 --output=$@

final: $(aligned-image) | $(tpvdir)





# Directories
$(BDIR):; mkdir $@

$(tpvdir) $(regdir) $(indir): | $(BDIR)
	mkdir $@

clean:; rm -rf $(BDIR)
